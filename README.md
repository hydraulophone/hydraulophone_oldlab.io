## To edit the website:

STEP 1. clone the repo locally

```
git clone https://gitlab.com/hydraulophone/hydraulophone.gitlab.io.git
cd hydraulophone.gitlab.io
```

STEP 2. start a new branch localy
```
git checkout -b <new_branch>
```

STEP 3. make changes and test website locally

STEP 4. check for change
```
git status
```

STEP 5. stage change
```
git add <files> or git add .
```

STEP 6. commit change
```
git commit -m "<some description>"
```

STEP 7. push change
```
git push origin <new_branch>
```
